# Code Snippet (CRUD Operations)

[![N|Sayegh1944](http://www.sayegh1944.com/images/logo.png)](http://www.sayegh1944.com/)

## Note
Please Use following class as needed before use the code below:
```
use oat\tao\model\TaoOntology;
use core_kernel_classes_Class;
use core_kernel_classes_Property;
use tao_models_classes_LanguageService;
use core_kernel_classes_Resource;
use oat\generis\model\OntologyRdfs;
use oat\generis\model\user\UserRdf;
use oat\generis\model\GenerisRdf;
use core_kernel_users_Service;
use tao_models_classes_dataBinding_GenerisFormDataBinder;
use oat\tao\model\event\UserUpdatedEvent;
use oat\taoTestTaker\models\events\TestTakerRemovedEvent;
use oat\taoDeliveryRdf\model\DeliveryFactory;
use oat\taoDeliveryRdf\model\DeliveryAssemblyService;


```
Please Use following code after defining class:
```
use EventManagerAwareTrait;
```


---
## Items

### Read All Items
The following code get all items in TAO.
```
$itemClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_ITEM);
$itemModels = $itemClass->getInstances();

$ItemsNameArray = [];
foreach ($itemModels as $itemModel) {
    $ItemsNameArray[] = $itemModel->getLabel();
}
```

### Create New  Item

The following code to create new delivery in TAO.
```
$itemClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_ITEM);
$newItem = $itemClass->createInstance('New Item Instance');
```
### Update Item

The following code to create new delivery in TAO.
```
$itemClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_ITEM);
$propertyLabel = new core_kernel_classes_Property(RDFS_LABEL); // http://www.w3.org/2000/01/rdf-schema#label
$itemModels = $itemClass->getInstances();
foreach ($itemModels as $itemModel) {
    if ($itemModel->getLabel() == "Item Instance") {
        $itemModel->setPropertyValue($propertyLabel, 'Updated Item Instance');
    }
}
```
### Delete Item

The following code to create new delivery in TAO.
```
$itemClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_ITEM); 
$itemModels = $itemClass->getInstances();

foreach ($itemModels as $itemModel) {
    if ($itemModel->getLabel() == "Item Instance") {
        $itemModel->delete();
    }
}
```

---
## Test

### Read All Tests
The following code get all tests in TAO.
```
$itemClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_TEST); 
$itemModels = $itemClass->getInstances();

$ItemsNameArray = [];
foreach ($itemModels as $itemModel) {
    $ItemsNameArray[] = $itemModel->getLabel();
}
```

### Create New Test

The following code to create new test in TAO.
```
$itemClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_TEST);
$newItem = $itemClass->createInstance('New Test Instance');
```
### Update Test

The following code to create new test in TAO.
```Items
$itemClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_TEST); 
$propertyLabel = new core_kernel_classes_Property(RDFS_LABEL); 
$itemModels = $itemClass->getInstances();

foreach ($itemModels as $itemModel) {
    if ($itemModel->getLabel() == "New Test Instance") {
        $itemModel->setPropertyValue($propertyLabel, 'Updated Test Instance');
    }
}
```
### Delete Item

The following code to create new test in TAO.
```
$itemClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_ITEM); 
$itemModels = $itemClass->getInstances();

foreach ($itemModels as $itemModel) {
    if ($itemModel->getLabel() == "Updated Test Instance") {
        $itemModel->delete();
    }
}
```

---
## Test Takers

### Read All Test-Takers
The following code get all tests in TAO.
```
$itemClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_SUBJECT);
$itemModels = $itemClass->getInstances();

$ItemsNameArray = [];
foreach ($itemModels as $itemModel) {
    $ItemsNameArray[] = $itemModel->getLabel();
}
```

### Create New Test-Takers

The following code to create new test in TAO.
```
$testTakerLabel = 'test label';
$firstName = 'First Name From Code';
$lastName = 'Last Name From Code';
$testTakerEmail = 'code@code.com';
$testTakerUILanguage = tao_models_classes_LanguageService::singleton()->getLanguageByCode(DEFAULT_LANG)->getUri();
$loginName = date("His");
$PlainPassword = 'pa$$wOrd';

$itemClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_SUBJECT);
$newItem = $itemClass->createInstance('New Test Taker Instance' . date("H:i:s Y-m-d"));
$user = new core_kernel_classes_Resource($newItem->getURI());

$values[OntologyRdfs::RDFS_LABEL] = $testTakerLabel;
$values[UserRdf::PROPERTY_FIRSTNAME] = $firstName;
$values[UserRdf::PROPERTY_LASTNAME] = $lastName;
$values[UserRdf::PROPERTY_MAIL] = $testTakerEmail;
$values[UserRdf::PROPERTY_UILG] = $testTakerUILanguage;
$values[UserRdf::PROPERTY_LOGIN] = $loginName;
$values[GenerisRdf::PROPERTY_USER_PASSWORD] = \core_kernel_users_Service::getPasswordHash()->encrypt($PlainPassword);

$binder = new \tao_models_classes_dataBinding_GenerisFormDataBinder($user);
$binder->bind($values);


$tempValue = $this->getEventManager()->trigger(
    new UserUpdatedEvent(
        $user,
        $values)
);

$roleProperty = new \core_kernel_classes_Property(GenerisRdf::PROPERTY_USER_ROLES);
$subjectRole = new \core_kernel_classes_Resource(TaoOntology::PROPERTY_INSTANCE_ROLE_DELIVERY);
$user->setPropertyValue($roleProperty, $subjectRole);
```
### Update Test-Takers
The following code to update Test-Takers in TAO.
```
$URIResourceNeedToUpdate = "http://tao-ext.local/tao.rdf#i165683478555701167";

$user = new core_kernel_classes_Resource($URIResourceNeedToUpdate);

$testTakerLabel = 'updated test label';
$firstName = 'updated First Name From Code';
$lastName = 'updated Last Name From Code';
$testTakerEmail = 'updated code@code.com';
$testTakerUILanguage = tao_models_classes_LanguageService::singleton()->getLanguageByCode(DEFAULT_LANG)->getUri();
$PlainPassword = '@asD1234';


$values[OntologyRdfs::RDFS_LABEL] = $testTakerLabel;
$values[UserRdf::PROPERTY_FIRSTNAME] = $firstName;
$values[UserRdf::PROPERTY_LASTNAME] = $lastName;
$values[UserRdf::PROPERTY_MAIL] = $testTakerEmail;
$values[UserRdf::PROPERTY_UILG] = $testTakerUILanguage;
$values[GenerisRdf::PROPERTY_USER_PASSWORD] = \core_kernel_users_Service::getPasswordHash()->encrypt($PlainPassword);

$binder = new \tao_models_classes_dataBinding_GenerisFormDataBinder($user);
$binder->bind($values);


$tempValue = $this->getEventManager()->trigger(
    new UserUpdatedEvent(
        $user,
        $values
    )
);
```

### Delete Test-Takers
The following code to delete Test-Takers in TAO.

```
$URIResourceNeedToUpdate = "http://tao-ext.local/tao.rdf#i165683478555701167";

$user = new core_kernel_classes_Resource($URIResourceNeedToUpdate);
if (! is_null($user)) {
    $this->getEventManager()->trigger(new TestTakerRemovedEvent($URIResourceNeedToUpdate);
    $user->delete();
}
```
---
## Groups

### Read All Groups
The following code get all Groups in TAO.
```
$groupClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_GROUP);
$groupModels = $groupClass->getInstances();

$ItemsNameArray = [];
foreach ($groupModels as $groupModel) {
    $ItemsNameArray[] = $groupModel->getLabel();
}
```

### Create New Group

The following code to create new group in TAO.
```
$groupClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_GROUP);
$newGroup = $groupClass->createInstance('New Group Instance');
```
### Update Group

The following code to create new group in TAO.
```
$groupClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_GROUP);
$propertyLabel = new core_kernel_classes_Property(RDFS_LABEL); // http://www.w3.org/2000/01/rdf-schema#label

$groupModels = $groupClass->getInstances();
foreach ($groupModels as $groupModel) {
    if ($groupModel->getLabel() == "New Group Instance") {
        $groupModel->setPropertyValue($propertyLabel, 'Updated Group Instance');
    }
}
```
### Delete Group

The following code to create new group in TAO.
```
$GroupClass = new core_kernel_classes_Class(TaoOntology::CLASS_URI_GROUP);
$GroupModels = $GroupClass->getInstances();

foreach ($GroupModels as $GroupModel) {
    if ($GroupModel->getLabel() == "Group 1") {
        $GroupModel->delete();
    }
}
```

---

## Delivery

### Read All Deliveries Label
The following code get all deliveries in TAO.
```
$service = DeliveryAssemblyService::singleton();
$deliveries = $service->getAllAssemblies();

$deliveryNameArray = [];

foreach($deliveries as $delivery){
    $deliveryNameArray[] = $delivery->getLabel();
}
```

### Create New  Delivery

The following code create new delivery in TAO.
```
$URI_ASSEMBLED_DELIVERY = 'http://www.tao.lu/Ontologies/TAODelivery.rdf#AssembledDelivery';
$URI_TEST = 'http://tao-ext.local/tao.rdf#i165599489055512';

$deliveryFactory = $this->getServiceLocator()->get(DeliveryFactory::SERVICE_ID);

$assembledDeliveryClass = new core_kernel_classes_Class($URI_ASSEMBLED_DELIVERY);
$testResource = new core_kernel_classes_Resource($URI_TEST);
$label = "Test Delivery"; 

$newDelivery = $deliveryFactory->create($assembledDeliveryClass, $testResource, $label);
```

### Update Delivery label

The following code update delivery label in TAO.
```
$URI_DELIVERY = 'http://tao-ext.local/tao.rdf#i165684397486411224';

$deliveryClass = new core_kernel_classes_Class($URI_DELIVERY);
$propertyLabel = new core_kernel_classes_Property(RDFS_LABEL); // http://www.w3.org/2000/01/rdf-schema#label
$deliveryClass->editPropertyValues($propertyLabel, 'Updated Delivery');
```

### Delete Delivery

The following code delete delivery in TAO.
```
$URI_DELIVERY = 'http://tao-ext.local/tao.rdf#i165684397486411224';

$deliveryClass = new core_kernel_classes_Class($URI_DELIVERY);

if (! is_null($deliveryClass)) {
    $deliveryClass->delete(true);
}
```

---

## Hints

If you want to delete all the statements referencing the instance across the database as well, set the parameter $deleteReference to true:
```
$resource->delete(true);
```



