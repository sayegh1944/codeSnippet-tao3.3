# Extensions
- How to make a new extension in TAO 3.3 [Link](https://github.com/oat-sa/taohub-articles/blob/master/forge/How%20to%20make%20a%20new%20extension%203%203/how-to-make-a-new-extension-3-3.md)

- How to make a new extension 3 0 [Link](https://github.com/oat-sa/taohub-articles/blob/1d7aed57cd030fc6908b98046d05d0f159c9a1d2/forge/TAO%203%200/how-to-make-a-new-extension-3-0.md)

- Make a new extension 2.4 - 2.5 [Link](https://github.com/oat-sa/taohub-articles/blob/1d7aed57cd030fc6908b98046d05d0f159c9a1d2/forge/Tutorials/make-a-new-extension.md)

- Make a new extension 2.6 [Link](https://github.com/oat-sa/taohub-articles/blob/1d7aed57cd030fc6908b98046d05d0f159c9a1d2/forge/How-to-make-a-new-extension-2-6.md)

- How to make a new extension 3 0 v2 [Link](https://github.com/oat-sa/taohub-articles/blob/1d7aed57cd030fc6908b98046d05d0f159c9a1d2/forge/TAO%203%201/how-to-make-a-new-extension-3-0-v2.md)





# Templates
- structures.xml [Link](https://github.com/oat-sa/extension-tao-devtools/blob/407e3d470f354d5ec79e8f69964785a5e0aedba9/models/templates/controller/structures.xml.sample)

# General Topics
- Generis Overview [Link](https://github.com/oat-sa/taohub-articles/blob/1d7aed57cd030fc6908b98046d05d0f159c9a1d2/forge/Documentation%20for%20core%20components/generis-overview.md)

- Accessing the generis persistence layer [Link](https://github.com/oat-sa/taohub-articles/blob/1d7aed57cd030fc6908b98046d05d0f159c9a1d2/forge/Tutorials/how-to-access-the-generis-persistence-layer.md)

- Helpers - Form Generation [Link](https://github.com/oat-sa/taohub-articles/blob/1d7aed57cd030fc6908b98046d05d0f159c9a1d2/forge/Tutorials/how-to-access-the-generis-persistence-layer.md)

- URL Mapping, Actions structure, Workflow, structure.xml definition [Link](https://github.com/oat-sa/taohub-articles/blob/1d7aed57cd030fc6908b98046d05d0f159c9a1d2/forge/Documentation%20for%20core%20components/controllers.md)

- How to add a new items type [Link](https://github.com/oat-sa/taohub-articles/blob/1d7aed57cd030fc6908b98046d05d0f159c9a1d2/forge/Tutorials/how-to-add-a-new-items-type.md)




